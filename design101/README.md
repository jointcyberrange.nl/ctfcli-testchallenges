# Design101

Author: [Mathias Bjorkqvist](https://gitlab.com/mabjorkq)  

## Description

JCR scholencompetitie 2022, TS106 Secure by Design, Dag 1

In this challenge, you will be connecting to a network router.
This is not a new, fancy router with a nice web user interface - no, this is an old device, which is accessed and managed through an old-school command-line telnet user interface.
On this device, a previous user managed to store some important information - the flag for this challenge!
Fortunately for you, the password security of the device is as old as the user interface.
Your task is to connect to the device and log in.
Once logged in successfully, the flag will automatically be displayed.

## Learning objective

Security by design and security in depth, using access control, cryptographic protection techniques, and secure passwords.

## Hints

Hint #1:
The description does not mention anything about the username or password, but these are both needed to log in to the device.
How could you figure out what they may be?

Hint #2:
What information do you have about the device?
-It is an (old) network router.
-You can connect to it using the telnet protocol (tcp/23).
Do you get more information when you try to connect?

Hint #3:
When connecting to the device over telnet, the device displays a banner.
What does this banner tell you?
What about if you search for more information online, using the information in the banner?

Hint #4:
The banner contains the brand name and model of the network router.
If you search online, you can find the default username and password for the router.
This can be used to log in to the device and retrieve the flag.

## Quiz

It is quite common for people to use the same (email address and) password on many different web sites.
Is this a good or a bad idea, and why?

a) Good idea - less things to remember
b) Bad idea - the password requirements are different on different websites
c) Bad idea - if one account is compromised, the same information can be used to compromise other accounts with the same username and password

Correct answer: c

## Requirements

This is a container challenge!


## Exploit

A Docker container is set up with telnet enabled, to emulate an (old) network router.
When connecting over telnet, the server identifies itself with a model name and version number.
The default username and password can be found by searching online.
After logging as with the default user, the flag will be displayed.

The flag is:

```
JCR(z1on0101)
```

Testing commit
