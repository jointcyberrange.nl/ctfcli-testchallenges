# Mr. Rami

Author: [Rohan Mukherjee](https://github.com/roerohan)  
Editor: [Mike van den Brink](https://github.com/mvdb0110)

## Description

```
"People who get violent get that way because they can’t communicate."
```

## Requirements

- Knowledge of `robots.txt`.


## Exploit

When you google the challenge description, you find out that the quote is from Mr. Robot. This indicates that the user might want to check out the `robots.txt` for the website.
<br />

When you open the website, it serves the `index.html` file, which has content written about `Brobot`, again trying to put across `robots.txt`. When you visit `robots.txt`, you see:

```
# Hey there, you're not a robot, yet I see you sniffing through this file.
# SEO you later!
# Now get off my lawn.

Disallow: /fade/to/black
```

When you visit the disallowed route, you get the flag!
<br />

The flag is:

```
CTF{br0b0t_1s_pr3tty_c00l_1_th1nk}
```
