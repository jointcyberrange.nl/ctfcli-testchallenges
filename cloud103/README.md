# Cloud103

Author: [Daniel Meinsma](https://github.com/djmhhs)  

## Description

JCR scholencompetitie 2022, TS103 Cloud Security, Dag 3

Use SSH to connect to one of the 3 IP addresses, on the nodePort.
Credentials = unpriv:unpriv 

This challenge is about escaping a chroot jail. The flag is located in the root folder. After using SSH, run ./start_container in the home folder.

## Requirements

This is a container challenge!


## Exploit

https://filippo.io/escaping-a-chroot-jail-slash-1/

The flag is:

```
JCR(are_you_on_cloud_nine)
```

Testing commit 2
