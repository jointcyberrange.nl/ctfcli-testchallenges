# ctfcli-testchallenges

## Description

Testset and workflows for repository based challenge deployment. Builds on `ctfd.io` and `ctfcli`.
It is an example, and can be used to test setups.

## Workflow Get API access to a CTFd test environment

Objective: get a test environment to deploy challenges to.
This can be done locally, or you may have one remotely, possibly at <https://demo.ctfd.io>. An approach with plugins is described here: <https://gitlab.com/jointcyberrange.nl/ctfd-docker-with-plugins>.
Docker is needed for this workflow.

1. Get an API key through these instructions <https://docs.ctfd.io/docs/api/getting-started/#access-token>, hint: it is at the /settings page.

## Workflow Convert Challenge to CTFcli Format

Objective: create a new challenge in a repository.

Prerequisite: CTFd API access.

1. Clone this repository and create a branch, for example, named: `add-md5sum-challenge`
2. Create a folder in the root of the repository with the name of the challenge: `md5sum`
3. Add a `README.md` and place the content found under "Message" in the challenge web interface into this file.
4. Create a `challenge.yml` file in the folder, using the following template as a starting point: [challenge.yml template](https://gitlab.com/jointcyberrange.nl/ctfcli-challenges-as-code-public/-/blob/main/crypto/challenge.yml?ref_type=heads)
5. Include the metadata from the challenge web interface in the `challenge.yml` file. (Set attempts to 0 for unlimited attempts)
6. If there are source code files, add them to a `src` folder and reference them in the `challenge.yml` under `files:`
7. Optionally, add Topics and Tags (if not present in the challenge web interface)
8. Include any other files if they exist.
9. Test the challenge (workflow described below).
10. If the challenge works, commit and push to the branch.
11. Create a merge request to merge into the main branch.

## Workflow test ctfcli challenge

Testing ctfcli requires an operational CTFd instance, see other workflow.

1. Install ctfcli: `pip install ctfcli` ; you probably want a Python venv first
2. Initialize ctfcli: `ctf init`
3. Enter CTFd instance URL: e.g. [http://localhost:8000](http://localhost:8000)
4. Enter CTFd Admin Access Token. See: [CTFd Access Token Documentation](https://docs.ctfd.io/docs/api/getting-started/#access-token)
5. If the challenge folder is in the same repository, add it using: `ctf challenge add [CHALLENGE FOLDER NAME]`, for example: `ctf challenge add md5sum`
6. Install the challenge with: `ctf challenge install [CHALLENGE FOLDER NAME]`, for example: `ctf challenge install md5sum`
7. The challenge should now be visible at [http://localhost:8000/challenges](http://localhost:8000/challenges)
8. Check if everything looks good (in terms of text formatting) and if the flag works
9. If you've made changes to `challenge.yml`, you can synchronize it using: `ctf challenge sync [CHALLENGE FOLDER NAME]`

## Workflow Container challenge deploy

Objective: to get a deployable container image in the proper registry.

Assume: challenge name / container image = chal-img

Assume: your registry name is `registry.gitlab.com`
Assume: you are logged in through e.g. `docker login registry.gitlab.com -u user`
Assume: your current host has the same architecture as the target environment (e.g. x86).

1. Goto the directory with the `Dockerfile`, run `docker build . -t chal-img`
2. Run `docker build -t registry.gitlab.com/jointcyberrange.nl/ctfcli-testchallenges/chal-img` .
3. Run `docker push registry.gitlab.com/jointcyberrange.nl/ctfcli-testchallenges/chal-img`
4. Your image path is now: `image: registry.gitlab.com/jointcyberrange.nl/ctfcli-testchallenges/chal-img`

## Manual Production Workflow

1. Ensure that the `.ctf/config` is configured correctly.
2. Possible commands:
    - Install all challenges:

        ```bash
        ls -1 -d */ | xargs -L 1 ctf challenge install
        ```

    - Synchronize all challenges:

        ```bash
        ls -1 -d */ | xargs -L 1 ctf challenge sync
        ```

## Automatic Production Workflow

This repository has scripting for automatic deployment, `.gitlab-ci.yml` has the details of that.
In the current version, you'll have edit the `.ctf/config` in Gitlab in order to define the destination of this.

## GITLAB boilerplate after this

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing (SAST)](https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)

***

## Editing this README

When you're ready to make this README your own, just edit this file and use the handy template below (or feel free to structure it however you want - this is just a starting point!). Thanks to [makeareadme.com](https://www.makeareadme.com/) for this template.

## Suggestions for a good README

Every project is different, so consider which of these sections apply to yours. The sections used in the template are suggestions for most open source projects. Also keep in mind that while a README can be too long and detailed, too long is better than too short. If you think your README is too long, consider utilizing another form of documentation rather than cutting out information.

## Badges

On some READMEs, you may see small images that convey metadata, such as whether or not all the tests are passing for the project. You can use Shields to add some to your README. Many services also have instructions for adding a badge.

## Visuals

Depending on what you are making, it can be a good idea to include screenshots or even a video (you'll frequently see GIFs rather than actual videos). Tools like ttygif can help, but check out Asciinema for a more sophisticated method.

## Installation

Within a particular ecosystem, there may be a common way of installing things, such as using Yarn, NuGet, or Homebrew. However, consider the possibility that whoever is reading your README is a novice and would like more guidance. Listing specific steps helps remove ambiguity and gets people to using your project as quickly as possible. If it only runs in a specific context like a particular programming language version or operating system or has dependencies that have to be installed manually, also add a Requirements subsection.

## Usage

Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.

## Support

Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.

## Roadmap

If you have ideas for releases in the future, it is a good idea to list them in the README.

## Contributing

State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## Authors and acknowledgment

Show your appreciation to those who have contributed to the project. Many students of the Hogeschool Utrecht. Shoutout to CTFd.

## License

For open source projects, say how it is licensed.

## Project status

If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.
