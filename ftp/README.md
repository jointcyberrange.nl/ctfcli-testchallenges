# Nesting

Author: [Daniel Meinsma](https://github.com/dmeinsma)  
Editor: [Mike van den Brink](https://github.com/mvdb0110)

## Description

```
Meet Alice. Alice knows Bob. Bob lends a hand.
```

## Requirements

- Knowledge of the File Transfer Protocol (FTP)
- Knowledge of the Secure Shell daemon (SSH)


## Exploit

VSFTPD en SSH

```
FLAG
```
