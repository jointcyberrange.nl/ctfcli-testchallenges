
<!DOCTYPE html>
<!--Sister HL-L2350DW Printer System © 2017-->
<!--Trademark Sister Industries, Ltd-->
<!--Country of Origin: Taiwan-->
<!--Class 1 Laser Product-->
<!--System information:-->
<!--Supported modes 802.11b,g,b, USB 2.0-->
<!--Supported services: Android Print Service Plugin, Apple AirPrint, Mopia Print Service, iPrint&Scan, WiFi-Direct-->
<!--Supported cartridges: DR-2400, TN-2420, TN-2410-->
<!--System MAC: 0C:1A:55:1F:13:D0-->
<!--System Memory: 536870912b-->
<!--Clock speed: 600MHz-->

<html>   
<head>  
<meta name="viewport" content="width=device-width, initial-scale=1">  
<title> WPS Page</title>  

<?php
	session_start();
	if (isset($_POST['pin'])) {
		$pin = $_POST['pin'];
		$pin_first = substr($pin,0,4);
		$pin_second = substr($pin,4,3);
		$hash_first = '$2y$10$U6LHjC0dENNK3nAYVGGmKu/qUr48rYEZuLNthxAPyYSU1ultWenNa';
		$hash_second = '$2y$10$lLLPaNTHtY9rqA2dDl34y.GRq47InV4au4j1uU7jwf5HYLcl6EMHG';
		$hash_flag = 'tQ/OFMVdaBzWNs54reCeP/C+Jg==';
		if (password_verify($pin_first, $hash_first)) {
			if (password_verify($pin_second, $hash_second)) {
				$flag = openssl_decrypt($hash_flag, $cipher_algo='aes-256-xts', substr($pin,0,7), $options=0, $iv='', $tag=null);
				echo "<p class=\"success\">Connection established! CTF($flag)</p>";
			} else {
				echo "<p>Err [0034]: Partial PIN $pin_first</p>";
			}
		} else {
			echo "<p>Incorrect PIN $pin</p>";
		}
	}
?> 



<style>   
Body {  
  font-family: Calibri, Helvetica, sans-serif;  
  background-color: white;  
}  
button {   
	   background-color: #A9A9A9;   
	   width: 100%;  
		color: black;   
		padding: 15px;   
		margin: 10px 0px;   
		border: none;   
		cursor: pointer;   
		 }   
 form {   
		border: 3px solid #f1f1f1;   
	}   
 input[type=text], input[type=password] {   
		width: 100%;   
		margin: 8px 0;  
		padding: 12px 20px;   
		display: inline-block;   
		border: 2px solid grey;   
		box-sizing: border-box;   
	}  
 button:hover {   
		opacity: 0.7;   
	}   
  .cancelbtn {   
		width: auto;   
		padding: 10px 18px;  
		margin: 10px 5px;  
	}   
		
	 
 .form-element {   
		padding: 25px;   
		background-color: lightgrey;  
	}   
</style>   
</head>    
<body>    
	<center> <h1> WPS Connect </h1> </center>   
	<form method="post" action="" name="login-form">  
		<div class="form-element">   
			<label>Please enter the 8-digit WPS PIN as found on the bottom of your device : </label>   
			<input type="text" placeholder="Enter PIN" name="pin" pattern="[0-9]+" required>
			<button type="submit" name="connect" value="pin">Connect</button>     
		</div>   
	</form>     
</body>     
</html>
