# IoT101

Author: [Joris Mellens](https://gitlab.com/JJMellens)  

## Description

The WPS challenge provides the challenger with a Docker container hosting a webserver on localhost:80. Solving the challenge involves either using optimized bruteforce that exploits the weakness of WPS' design (though a modified version of a regular attack will be needed, as the WPS protocol is merely replicated in a webserver-environment), or by using the container's MAC address to calculate the WPS PIN instead.

## User-facing challenge description

In the quest to retrieve our flag, your first objective will be to gain entry to the thief's home network and find more evidence about where they're keeping our stolen property. Their cameras and alarms prevent simply gaining physical access, but perhaps a flaw in the network security can be found. Scanning around for listening services, you find an IP-printer listening for a WPS PIN. Perhaps this can be an entry point?

## Learning objective

Security by design and security in depth, using access control, cryptographic protection techniques, and secure passwords.

## Hints

Hint #1
Cost: 20
	
WPS PINs are 8-digit codes, which would mean there's 10^8 different codes you'd have to iterate through if you wanted to bruteforce it. Read up on the properties of WPS; you may find the number to be substantially lower than 10^8...

-----

Hint #2
Cost: 60
	
When adding a hard-coded access code to a device, the worst thing you can do is to simply use the same "default password" for every device in that line of products. The best thing you can do is to randomly generate a completely unique code for every device. The right way to generate this unique code is by using a cryptographically secure algorithm. 
The wrong way to do it is by using an algorithm that only takes a single, easily determinable seed.

## Quiz

IoT devices have many difficulties to overcome when it comes to security, both due to the nature of the market, and the nature of the devices themselves. One such common difficulty is a lack of computational power. What essential element of cybersecurity is most affected by this lack of computational power?

correct answer: encryption

## Requirements

This is a container challenge!


## Exploit

The WPS challenge provides the challenger with a Docker container hosting a webserver on localhost:80, and tasks the challenger with cracking a WPS key. This can be solved either by learning that WPS keys can be calculated from the device's MAC address, finding the MAC, and calculating the PIN, or by clever application of a bruteforce attack (WPS PINs are 8 digits but the last digit is a mere checksum, and the actual checking of the input is done in two steps: first the first four digits, then the last four).

The flag is:

```
JCR(work_smart_not_hard)
```

Testing commit

