#!/bin/sh
socat \
    -T60 \
    TCP-LISTEN:12345,reuseaddr,fork \
    EXEC:"timeout 60 ./server.py"