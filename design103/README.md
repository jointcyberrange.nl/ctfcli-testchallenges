# Design103

Author: [Mathias Bjorkqvist](https://gitlab.com/mabjorkq)  

## Description

Passwords in computer systems should not be stored unprotected ("in the clear", or "in plaintext").
One common way to store passwords is through the use of cryptographic hash functions.
In case intruders hack a computer system and gain access to the passwords, they will then only gain access to the hashed passwords.
Password cracking involves trying to figure out what actual password a hashed password corresponds to.
This typically involves generating lots of passwords to see if, when hashed, they match the recovered hashed passwords.
Another option is to look up the hashed password in rainbow tables.

In this challenge, you will log in to a Linux system over SSH, using the admin credentials (username: admin, password: 1234).
The task is to find the flag stored in a file in the home directory of the user alice.

This task is similar to the previous task, but the system administrator has made fewer/less critical blunders.

## Learning objective

Security by design and security in depth, using access control, cryptographic protection techniques, and secure passwords.

## Hints

Hint #1:
The idea of this challenge is to figure out the password of the user alice, and log in as that user.
On recent Linux systems, the user passwords are stored in the /etc/shadow file.
Luckily for you, the system administrator has messed up, and made that file world-readable!
You can now read the protected password of alice.
How can you figure out what the unprotected password is, so that you can use it to log in to the system?

Hint #2:
In the previous challenge, a blunder had been made, as no salt had been used when hashing the password for the user alice.
This time, the password is stored in a state-of-the-art way, using a salt and the SHA-512 cryptographic hash function.
However, the security of the system is only as strong as its weakest link.
In this case, alice has used a weak password.
How can you find out what it is?

Hint #3:
Password cracking tools use short permutations of random characters, dictionaries with common passwords, and combinations and variations of these passwords, as ways to crack passwords.
Using a password cracking tool and a default dictionary, you should be able to crack the password for alice in under a minute, on "regular" modern consumer hardware.

## Quiz

Which of the following is the best way to come up with a strong password?

a) Take a common word and add a number
b) Use your birthday and the name of your pet
c) A long string of different characters in random order

Answer: c

## Requirements

This is a container challenge!


## Exploit

The setup is similar to challenge #2.
The main difference is that here, the password of the other user is stored in a state-of-the-art way; salted and hashed with SHA-512.
However, the password is weak.
The idea is to use a password cracking tool to obtain the password.
The default word list of John the Ripper is good enough, and the password should be cracked within a minute on regular modern end-user hardware.

The flag is:

```
JCR(joshua)
```

Testing commit
