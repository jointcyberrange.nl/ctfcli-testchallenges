# Cloud101

Author: [Daniel Meinsma](https://github.com/djmhhs)  

## Description

JCR scholencompetitie 2022, TS103 Cloud Security, Dag 1

Use SSH to connect to one of the 3 IP addresses, on the nodePort.
Credentials = unpriv:unpriv 

Inspired by the vulnerability CVE-2019-5736, describing a way to break out of docker containers via runc, several files have been placed in the root folder of the container. 

## Requirements

This is a container challenge!


## Exploit

cd /
nano monitor.sh
cat flag.txt >> output.txt
./runc_monitor
cat output.txt

The flag is:

```
JCR(bespin)
```

Testing commit
