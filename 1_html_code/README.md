# 1-html-code

# Solution

Visit the website ```http://{url}/htmlcode```, right click on the page and select __Inspect__. This shows the HTML code of the webpage. Expand the __body__ part and the flag will be shown.

Source: Marije
