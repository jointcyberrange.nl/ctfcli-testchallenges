# Cloud102

Author: [Daniel Meinsma](https://github.com/djmhhs)  

## Description

JCR scholencompetitie 2022, TS103 Cloud Security, Dag 2

Use SSH to connect to one of the 3 IP addresses, on the nodePort.
Credentials = unpriv:unpriv 

This challenge is about exploiting privileged container permissions. The flag is located in the root folder. 

## Requirements

This is a container challenge!


## Exploit

https://medium.com/@cyb0rgs/exploiting-privileged-container-permissions-part-1-2ed535988653

The flag is:

```
JCR(With_your_head_in_the_clouds)
```

Testing commit
