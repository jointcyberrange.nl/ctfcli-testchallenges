# Design102

Author: [Mathias Bjorkqvist](https://gitlab.com/mabjorkq)  

## Description

JPasswords in computer systems should not be stored unprotected ("in the clear", or "in plaintext").
One common way to store passwords is through the use of cryptographic hash functions.
In case intruders hack a computer system and gain access to the passwords, they will then only gain access to the hashed passwords.
Password cracking involves trying to figure out what actual password a hashed password corresponds to.
This typically involves generating lots of passwords to see if, when hashed, they match the recovered hashed passwords.
Another option is to look up the hashed password in rainbow tables.

In this challenge, you will log in to a Linux system over SSH, using the admin credentials (username: admin, password: 1234).
The task is to find the flag stored in a file in the home directory of the user alice.

## Learning objective

Security by design and security in depth, using access control, cryptographic protection techniques, and secure passwords.

## Hints

Hint #1:
The idea of this challenge is to figure out the password of the user alice, and log in as that user.
How can you find out the password?

Hint #2:
On recent Linux systems, the user passwords are stored in the /etc/shadow file.
Luckily for you, the system administrator has messed up, and made that file world-readable!
You can now read the protected password of alice.
How can you figure out what the unprotected password is, so that you can use it to log in to the system?

Hint #3:
In addition to making the /etc/shadow file world readable, the system administrator (or perhaps alice herself) has performed another blunder by using a sub-optimal mechanism to protect the password.
Have you tried simply searching online with the protected password string?

## Quiz

Q: What is a technique to thwart a rainbow table attack on stored, hashed passwords?

A: Salt

## Requirements

This is a container challenge!


## Exploit

The challenge is also implemented as a Docker container.
The instructions will provide the username and password of a regular user of the system.
The instructions will also say that the flag is stored in a file in the home directory of another user of the system.
The */etc/shadow* file has been made world readable, and the password of the other user is both weak, and stored unsalted and hashed using MD5.
The quickest way to figure out the password is to simply search for the encrypted password online.
Since it is not salted, it will show up on websites/forums.

The flag is:

```
JCR(idspispopd)
```

Testing commit
